
import tensorflow as tf
from jinja2 import Environment, FileSystemLoader, Template
import random
import numpy as np
from collections import deque
import xmltodict
import os
import math
import time

# Number of valid actions.
ACTIONS = 5*5

# Decay rate of past observations.
GAMMA = 0.99

# Timesteps to observe before training.
OBSERVE = 1000.

# Frames over which to anneal epsilon.
EXPLORE = 1000.

# Final value of epsilon.
FINAL_EPSILON = 0.05

# Starting value of epsilon.
INITIAL_EPSILON = 1.0

# Number of previous transitions to remember in the replay memory.
REPLAY_MEMORY = 5900

# Size of minibatch.
BATCH = 32

# Only select an action every Kth frame, repeat the same action
# for other frames.
K = 2

# Learning Rate.
Lr = 1e-4

# Action that will terminal the exploration
TERMINAL_ACTION = 12

# Number of FPGA board
NUM_FPGA = 10

FPGAs = [
    'xc7a200tfbg676-2',
    'xcvu9p-flga2104-2L-e-es1',
    'xcvu190-flgc2104-2-e',
    'xcvu095-ffva2104-2-e',
    'xcku040-ffva1156-2-e',
    'xc7z045ffg900-2',
    'xc7z020clg484-1',
    'xc7vx690tffg1761-2',
    'xc7vx485tffg1761-2',
    'xc7k325tffg900-2'
]

FPGA_DSP = [720,6840,1800,768,1920,900,220,3600,2800,840]

'''
AC7XX  xc7a200tfbg676-2         DSP 720
VCU118 xcvu9p-flga2104-2L-e-es1 DSP 6840 
VCU110 xcvu190-flgc2104-2-e     DSP 1800
VCU108 xcvu095-ffva2104-2-e     DSP 768
KCU105 xcku040-ffva1156-2-e		DSP 1920
ZC706 xc7z045ffg900-2			DSP 900
Zedboard xc7z020clg484-1		DSP 220
VC709 xc7vx690tffg1761-2		DSP 3600 
VC707 xc7vx485tffg1761-2		DSP 2800
KC705 xc7k325tffg900-2			DSP 840
'''

# Number of parameter #####################################
#NUM_p1 = 10 # knum

#NUM_p2 = 8 # ksize

NUM_dim1 = 9 # matrix dim1[16~4096]

NUM_dim2 = 9 # matrix dim2[16~4096]

# a list to hold the history, will be dumped regularly
history = np.load('./history_trace.npy')[()]
print('history loaded!')
#history = dict()

def weight_variable(shape):
    """ Initializa the weight variable."""
    initial = tf.truncated_normal(shape, stddev=0.01)
    return tf.Variable(initial)


def bias_variable(shape):
    """ Initializa the bias variable."""
    initial = tf.constant(0.01, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W, stride):
    """ Define a convolutional layer."""
    return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding="SAME")


def max_pool_2x2(x):
    """ Define a maxpooling layer."""
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")


def createNetwork():
    """ Create a convolutional network for estimating the Q value.
    Args:
    Returns:
        s: Input layer
        readout: Output layer with the Q-values for every possible action

    """
    # Initialize the network weights and biases.
    '''
    W_conv1 = weight_variable([8, 8, 4, 32])
    b_conv1 = bias_variable([32])

    W_conv2 = weight_variable([4, 4, 32, 64])
    b_conv2 = bias_variable([64])

    W_conv3 = weight_variable([3, 3, 64, 64])
    b_conv3 = bias_variable([64])
    '''

    W_fc1 = weight_variable([16, 128])
    b_fc1 = bias_variable([128])

    W_fc2 = weight_variable([128, 256])
    b_fc2 = bias_variable([256])

    W_fc3 = weight_variable([256, 128])
    b_fc3 = bias_variable([128])

    W_fc4 = weight_variable([128, ACTIONS])
    b_fc4 = bias_variable([ACTIONS])

    # Input layer.
    s = tf.placeholder("float", [None, 16])

    # Hidden layers.
    h_fc1 = tf.nn.relu(tf.matmul(s, W_fc1) + b_fc1) # exclude absolute latency here
    h_fc2 = tf.nn.relu(tf.matmul(h_fc1, W_fc2) + b_fc2)
    h_fc3 = tf.nn.relu(tf.matmul(h_fc2, W_fc3) + b_fc3)
    # Output layer
    readout = tf.matmul(h_fc3, W_fc4) + b_fc4

    return s, readout


def get_action_index(readout_t, epsilon, t):
    """ Choose an action epsilon-greedily.
    Details:
        choose an action randomly:
        (1) in the observation phase (t<OBSERVE).
        (2) beyond the observation phase with probability "epsilon".
        otherwise, choose the action with the highest Q-value.
    Args:
        readout_t: a vector with the Q-value associated with every action.
        epsilon: tempreture variable for exploration-exploitation.
        t: current number of iterations.
    Returns:
        index: the index of the action to be taken next.
    """

    action_index = 0
    if t < EXPLORE:
        action_index= np.random.randint(len(readout_t))
    else:
        a=np.random.rand(1)
        if a<epsilon:
            action_index= np.random.randint(len(readout_t))
        else:
            action_index= np.argmax(np.array(readout_t))
    return action_index

def scale_down_epsilon(epsilon, t):
    """ Decrease epsilon after by ((INITIAL_EPSILON - FINAL_EPSILON) / EXPLORE )
    in case epsilon is larger than the desired final epsilon or beyond
    the observation phase.
    Args:
        epsilon: the current value of epsilon.
        t: current number of iterations.
    Returns:
        the updated epsilon
    """

    if t >= OBSERVE and epsilon > FINAL_EPSILON:
        epsilon = epsilon - ((INITIAL_EPSILON - FINAL_EPSILON) / EXPLORE )
    return epsilon


def run_selected_action(a_t, s_t, board, param):
    """ Run the selected action and return the next state and reward.
    Do not forget that state is composed of the 4 previous frames.
    Hint: check the initialization for the interface to the game simulator.
    Args:
        a_t: current action.
        s_t: current state.
        board: FPGA board chosen ########################################################################################################
    Returns:
        s_t1: next state.
        r_t: reward.
        terminal: indicating whether the episode terminated (output of the simulator).
    """

    avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc, new_param, out_bound=hls_step(a_t, board,param)
    re_latency = (avg_latency-s_t[0])/(s_t[0]+0.001)
    re_dsp = (dsp_perc-s_t[6])/(s_t[6]+0.001)
    re_ff = (ff_perc - s_t[7]) /(s_t[7]+0.001)
    re_lut = (lut_perc-s_t[8])/(s_t[8]+0.001)
    re_bram = (bram_perc-s_t[9])/(s_t[9]+0.001)
    re_timing = (timing_perc-s_t[10])/(s_t[10]+0.001)
    s_t1 =np.array([avg_latency,FPGA_DSP[board],param["knum"],param["ksize"], param['matrix_dim1'], param['matrix_dim2'], dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc, re_latency, re_dsp,re_ff, re_lut, re_bram, re_timing])

    print('Relative values:')
    print(s_t1)
    #terminal definition
    if a_t == TERMINAL_ACTION:
        terminal = 1
    else:
        terminal = 0

    #reward function
    r_t = - (re_latency * 100)

    # out bound penalty
    if dsp_perc >1.0 or ff_perc > 1.0 or lut_perc >1.0 or bram_perc > 1.0 or timing_perc >1.0:
        r_t = - 1.0
    if out_bound:
        r_t = - 1.0

    return s_t1, r_t, terminal, new_param


def compute_cost(target_q, a_t, q_value):
    """ Compute the cost.
    Args:
        target_q: target Q-value.
        a_t: current action.
        q_value: current Q-value.
    Returns:
        cost
    """
    # Q-value for the action.
    readout_action = tf.reduce_sum(tf.multiply(q_value, a_t), reduction_indices=1)

    # Q-Learning Cost.
    cost = tf.reduce_mean(tf.square(target_q - readout_action))

    return cost


def compute_target_q(r_batch, readout_j1_batch, terminal_batch):
    """ Compute the target Q-value for all samples in the batch.
    Distinguish two cases:
    1. The next state is a terminal state.
    2. The next state is not a terminal state.
    Args:
        r_batch: batch of rewards.
        readout_j1_batch: batch of Q-values associated with the next state.
        terminal_batch: batch of boolean variables indicating the game termination.
    Returns:
        target_q_batch: batch of target Q values.

    Hint: distinguish two cases: (1) terminal state and (2) non terminal states
    """

    target_q_batch = []

    for i in range(0, len(terminal_batch)):
        # If the terminal state is reached, the Q-value is only equal to the reward.
        if terminal_batch[i]:
            target_q_batch.append(r_batch[i])
        else:
            target_q_batch.append(r_batch[i]+GAMMA*np.max(readout_j1_batch[i]))

    return np.array(target_q_batch)


def trainNetwork(s, readout, sess):
    """ Train the artificial agent using Q-learning to play the pong game.
    Args:
        s: the current state formed by 4 frames of the playground.
        readout: the Q value for each passible action in the current state.
        sess: session
    """

    # Placeholder for the action.
    a = tf.placeholder("float", [None, ACTIONS])

    # Placeholder for the target Q value.
    y = tf.placeholder("float", [None])

    # Compute the loss.
    cost = compute_cost(y, a, readout)

    # Training operation.
    train_step = tf.train.AdamOptimizer(Lr).minimize(cost)

    # Initialize the replay memory.
    D = deque()

    # Initialize the action vector.
    initial_act = 12
    #initial_act = random.choice(range(ACTIONS));##############################################

    # Initialize the fpga board.
    board = random.choice(range(NUM_FPGA))##############################################
    print('Board Selected:%s DSP:%d' % (FPGAs[board], FPGA_DSP[board]))
    os.system('rm -rf ./hls_proj/solution1/')

    # Initialize parameter minimum size: M[16][16] V[16]###############################
    m_dim1 =4+random.choice(range(NUM_dim1)) 
    m_dim2 =4+random.choice(range(NUM_dim2))
    dsp_limit = int(math.log2(FPGA_DSP[board]//4))
    ksize_limit = min(dsp_limit,int(m_dim2))
    ksize_log = random.choice(range(2,ksize_limit+1))
    knum_log = min(dsp_limit - ksize_log,m_dim1)
    ksize_value = int(2**ksize_log)
    knum_value = int(2**(random.choice(range(knum_log+1))))


    param ={"knum":knum_value,"ksize":ksize_value,"matrix_dim1":int(2**(m_dim1)),"matrix_dim2":int(2**(m_dim2))}################################
    # Initialize the state of the game.
    print(param)
    avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc,param, out_bound = hls_step(initial_act, board,param)
    # [latency, dsp,ff,lut,bram, timing, relative latency, rel dsp,rel_ff,rel_lut,rel_bram, rel_timing]############################
    s_t = np.array([avg_latency,FPGA_DSP[board],param["knum"],param["ksize"], param['matrix_dim1'], param['matrix_dim2'], dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc, 0, 0,0,0,0,0])
    # Save and load model checkpoints.
    saver = tf.train.Saver()
    sess.run(tf.initialize_all_variables())
    checkpoint = tf.train.get_checkpoint_state("saved_networks_q_learning")
    if checkpoint and checkpoint.model_checkpoint_path:
        saver.restore(sess, checkpoint.model_checkpoint_path)
        print("Successfully loaded:", checkpoint.model_checkpoint_path)
    else:
        print("Could not find old network weights")

    # Initialize the epsilon value for the exploration phase.
    epsilon = INITIAL_EPSILON

    # Initialize the iteration counter.
    t = 0

    while True:
        # Choose an action epsilon-greedily.
        readout_t = readout.eval(feed_dict={s: [s_t[1:]]})[0]

        action_index = get_action_index(readout_t, epsilon, t)

        # a_t is one-hot encoded
        a_t = np.zeros([ACTIONS])

        a_t[action_index] = 1

        # Scale down epsilon during the exploitation phase.
        epsilon = scale_down_epsilon(epsilon, t)

        '''

        # Run the selected action and update the replay memeory
        for i in range(0, K):
            # Run the selected action and observe next state and reward.
            s_t1, r_t, terminal = run_selected_action(a_t, s_t, board)

            if terminal:
                board = random.choice(range(NUM_FPGA)); ###################################################

            # Store the transition in the replay memory D.
            D.append((s_t, a_t, r_t, s_t1, terminal))
            if len(D) > REPLAY_MEMORY:
                D.popleft()

        '''
        # Run the selected action and observe next state and reward.
        s_t1, r_t, terminal, param = run_selected_action(action_index, s_t, board, param)

        # Store the transition in the replay memory D.
        D.append((s_t[1:], a_t, r_t, s_t1[1:], terminal))
        if len(D) > REPLAY_MEMORY:
            D.popleft()

        # Start training once the observation phase is over.
        if (t > OBSERVE):

            # Sample a minibatch to train on.
            minibatch = random.sample(list(D), BATCH)

            # Get the batch variables.
            s_j_batch = [d[0] for d in minibatch]
            a_batch = [d[1] for d in minibatch]
            r_batch = [d[2] for d in minibatch]
            s_j1_batch = [d[3] for d in minibatch]
            terminal_batch = [d[4] for d in minibatch]

            # Compute the target Q-Value
            readout_j1_batch = readout.eval(feed_dict={s: s_j1_batch})
            target_q_batch = compute_target_q(r_batch, readout_j1_batch, terminal_batch)

            # Perform gradient step.
            train_step.run(feed_dict={
                y: target_q_batch,
                a: a_batch,
                s: s_j_batch})

        if terminal:
            # Initialize the fpga board.
            board = random.choice(range(NUM_FPGA))##############################################
            print('Board Selected:%s DSP:%d' % (FPGAs[board], FPGA_DSP[board]))
            os.system('rm -rf ./hls_proj/solution1/')
            # Initialize parameter minimum size: M[16][16] V[16]###############################
            m_dim1 =4+random.choice(range(NUM_dim1))
            m_dim2 =4+random.choice(range(NUM_dim2))
            dsp_limit = int(math.log2(FPGA_DSP[board]//4))
            ksize_limit = min(dsp_limit,int(m_dim2))
            ksize_log = random.choice(range(2,ksize_limit+1))
            knum_log = min(dsp_limit - ksize_log,m_dim1)
            ksize_value = int(2**ksize_log)
            knum_value = int(2**(random.choice(range(knum_log+1))))

            param ={"knum":knum_value,"ksize":ksize_value,"matrix_dim1":int(2**(m_dim1)),"matrix_dim2":int(2**(m_dim2))}################################
            # Initialize the state of the game.
            print(param)
            avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc,param, out_bound = hls_step(initial_act, board, param)
            # [latency, dsp,ff,lut,bram, timing, relative latency, rel dsp,rel_ff,rel_lut,rel_bram, rel_timing]############################
            # Mang: Changed s_t to s_t1 here, not sure if this is correct
            s_t1 = np.array([avg_latency,FPGA_DSP[board],param["knum"],param["ksize"], param['matrix_dim1'], param['matrix_dim2'], dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc, 0, 0,0,0,0,0])
		    
        # Update the state.
        s_t = s_t1

        # Update the number of iterations.
        t += 1

        # Save a checkpoint every 10000 iterations.
        if t % 10 == 0:
            saver.save(sess, 'saved_networks_q_learning/-dqn', global_step=t)
            np.save('./history_trace', history)

        # Print info.
        state = ""
        if t <= OBSERVE:
            state = "observe"
        elif t > OBSERVE and t <= OBSERVE + EXPLORE:
            state = "explore"
        else:
            state = "train"
        print("TIMESTEP", t, "/ STATE", state, "/ EPSILON", epsilon, "/ ACTION", action_index, "/ REWARD", r_t, "/ Q_MAX %e" % np.max(readout_t))


def generate_code(template_file, output_file_path, render_dict=None):
    '''
    Generates a string from a template file.
    '''
    cwd = os.getcwd()
    template_dir = os.path.join(cwd, "templates")
    template_env = Environment(loader=FileSystemLoader(template_dir), trim_blocks=True,
                                      keep_trailing_newline=True)

    template = template_env.get_template(template_file)
    if render_dict is not None:
        output_as_string = template.render(render_dict)
    else:
        output_as_string = template.render()

    with open(output_file_path, 'w') as generate_script_file:
        generate_script_file.write(output_as_string)

def hls_step(action_index, board, param): #board interface
    # execute vivado HLS
    act1 = action_index%5
    act2 = (action_index//5)%5
    out_bound = 0

    print("ACTION: %d -> %d %d" % (action_index ,act1, act2))
    param["knum"]=int(param["knum"]*(2.0**(act1-2.0)))
    param["ksize"]=int(param["ksize"]*(2.0**(act2-2.0)))

    print('Configuration before clamping: knum - %d ksize - %d' % (param['knum'], param['ksize']))

    if param["ksize"] *4 > FPGA_DSP[board]: # DSP limitation #########################################
        param["ksize"] = int(2**int(math.log2(FPGA_DSP[board]//4)))
        param["knum"] = 1
        print('ksize out of bound')
        out_bound = 1

    if param["knum"] * param["ksize"] *4 > FPGA_DSP[board]:
        param["knum"] =int(2**int(math.log2(FPGA_DSP[board]//(4*param["ksize"]))))
        print('knum out of bound')
        out_bound = 1

    if param["knum"] > param["matrix_dim2"]:
        param["knum"] = param["matrix_dim2"]
        out_bound = 1
    elif param["knum"] < 1:
        param["knum"] = 1
        out_bound =1

    if param["ksize"] > param["matrix_dim1"]:
        param["ksize"] = param["matrix_dim1"]
        out_bound = 1
    elif param["ksize"] < 4:
        param["ksize"] = 4
        out_bound = 1



    print('Final configuration: knum - %d ksize - %d' % (param['knum'], param['ksize']))

    ###############################ADDING tempate
    # generate paramterized code
    #generate_code("nmt.jinja", "./hls_proj/gened.cpp", param)

    if (FPGA_DSP[board],param["knum"],param["ksize"], param['matrix_dim1'], param['matrix_dim2']) in history.keys():
        print('Found in history!')
        (avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc) = history[(FPGA_DSP[board],param["knum"],param["ksize"], param['matrix_dim1'], param['matrix_dim2'])]
    else:
        os.system("python3 %s %d %d %d %d > %s" %
                  ('./tempate.py',
                   param['knum'],
                   param['ksize'],
                   param['matrix_dim1'],
                   param['matrix_dim2'],
                   './hls_proj/gened.cpp'
                   ))

        # generate tcl script for the selected FPGA
        generate_code("tcl_script.jinja", "./script.tcl", {'part': FPGAs[board]})

        start_time = time.time()
        hls_exit_code = os.system('/opt/Xilinx/Vivado_HLS/2017.1/bin/vivado_hls -f script.tcl > synth.log')
        end_time = time.time()
        elapsed_time = end_time - start_time
        print('HLS runs for:')
        print(elapsed_time)

        if hls_exit_code is not 0:
            raise Exception("hls did not exit normally!")

        with open('./hls_proj/solution1/syn/report/mvm_csynth.xml') as fd:
            doc = xmltodict.parse(fd.read())

        avg_latency = int(doc['profile']['PerformanceEstimates']['SummaryOfOverallLatency']['Average-caseLatency'])

        dsp = int(doc['profile']['AreaEstimates']['Resources']['DSP48E'])
        ff = int(doc['profile']['AreaEstimates']['Resources']['FF'])
        lut = int(doc['profile']['AreaEstimates']['Resources']['LUT'])
        bram = int(doc['profile']['AreaEstimates']['Resources']['BRAM_18K'])
        timing = float(doc['profile']['PerformanceEstimates']['SummaryOfTimingAnalysis']['EstimatedClockPeriod'])

        dsp_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['DSP48E'])
        ff_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['FF'])
        lut_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['LUT'])
        bram_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['BRAM_18K'])
        timing_avail = float(doc['profile']['UserAssignments']['TargetClockPeriod'])

        dsp_perc = dsp / dsp_avail
        ff_perc = ff / ff_avail
        lut_perc = lut / lut_avail
        bram_perc = bram / bram_avail
        timing_perc = timing/timing_avail

        history[(FPGA_DSP[board], param["knum"], param["ksize"], param['matrix_dim1'], param['matrix_dim2'])] = \
            (avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc)

    print('avg_latency: %d' % avg_latency)

    return avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc, param, out_bound #####################################

def playGame():
    """Paly the pong game"""

    # configure HLS environment
    os.system('source /opt/Xilinx/Vivado_HLS/2017.1/bin/setupEnv.sh')

    # Start an active session.
    sess = tf.InteractiveSession()

    # Create the network.
    s, readout = createNetwork()

    # Q-Learning
    s, readout = trainNetwork(s, readout, sess)


def main():
    """ Main function """
    playGame()


if __name__ == "__main__":
    main()
