
from jinja2 import Environment, FileSystemLoader, Template
import random
import numpy as np
from collections import deque
import xmltodict
import os
import math
import time
import matplotlib.pyplot as plt
# Number of FPGA board
NUM_FPGA = 10

FPGAs = [
    'xc7a200tfbg676-2',
    'xcvu9p-flga2104-2L-e-es1',
    'xcvu190-flgc2104-2-e',
    'xcvu095-ffva2104-2-e',
    'xcku040-ffva1156-2-e',
    'xc7z045ffg900-2',
    'xc7z020clg484-1',
    'xc7vx690tffg1761-2',
    'xc7vx485tffg1761-2',
    'xc7k325tffg900-2'
]

FPGA_DSP = [720,6840,1800,768,1920,900,220,3600,2800,840]

'''
AC7XX  xc7a200tfbg676-2         DSP 720
VCU118 xcvu9p-flga2104-2L-e-es1 DSP 6840 
VCU110 xcvu190-flgc2104-2-e     DSP 1800
VCU108 xcvu095-ffva2104-2-e     DSP 768
KCU105 xcku040-ffva1156-2-e		DSP 1920
ZC706 xc7z045ffg900-2			DSP 900
Zedboard xc7z020clg484-1		DSP 220
VC709 xc7vx690tffg1761-2		DSP 3600 
VC707 xc7vx485tffg1761-2		DSP 2800
KC705 xc7k325tffg900-2			DSP 840
'''

NUM_dim1 = 9 # matrix dim1[16~4096]

NUM_dim2 = 9 # matrix dim2[16~4096]

# a list to hold the history, will be dumped regularly
graph_data = []

def generate_code(template_file, output_file_path, render_dict=None):
    '''
    Generates a string from a template file.
    '''
    cwd = os.getcwd()
    template_dir = os.path.join(cwd, "templates")
    template_env = Environment(loader=FileSystemLoader(template_dir), trim_blocks=True,
                                      keep_trailing_newline=True)

    template = template_env.get_template(template_file)
    if render_dict is not None:
        output_as_string = template.render(render_dict)
    else:
        output_as_string = template.render()

    with open(output_file_path, 'w') as generate_script_file:
        generate_script_file.write(output_as_string)

def search(board, m_dim1, m_dim2): #board interface
    ksize = 4
    knum  = 1

    while ksize < m_dim1 :
        while knum < m_dim2:
            if knum * ksize * 4 < FPGA_DSP[board] :
                print('ksize=%d, knum=%d' % (ksize, knum))
                os.system("python3 %s %d %d %d %d > %s" %
                          ('./tempate.py',
                           knum,
                           ksize,
                           m_dim1,
                           m_dim2,
                           './hls_proj/gened.cpp'
                           ))

                # generate tcl script for the selected FPGA
                generate_code("tcl_script.jinja", "./script.tcl", {'part': FPGAs[board]})

                start_time = time.time()
                hls_exit_code = os.system('/opt/Xilinx/Vivado_HLS/2017.1/bin/vivado_hls -f script.tcl > synth.log')
                end_time = time.time()
                elapsed_time = end_time - start_time
                print('HLS runs for:')
                print(elapsed_time)

                if hls_exit_code is not 0:
                    print("hls did not exit normally!")
                else:
                    with open('./hls_proj/solution1/syn/report/mvm_csynth.xml') as fd:
                        doc = xmltodict.parse(fd.read())

                    avg_latency = int(doc['profile']['PerformanceEstimates']['SummaryOfOverallLatency']['Average-caseLatency'])

                    dsp = int(doc['profile']['AreaEstimates']['Resources']['DSP48E'])
                    ff = int(doc['profile']['AreaEstimates']['Resources']['FF'])
                    lut = int(doc['profile']['AreaEstimates']['Resources']['LUT'])
                    bram = int(doc['profile']['AreaEstimates']['Resources']['BRAM_18K'])
                    timing = float(doc['profile']['PerformanceEstimates']['SummaryOfTimingAnalysis']['EstimatedClockPeriod'])

                    dsp_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['DSP48E'])
                    ff_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['FF'])
                    lut_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['LUT'])
                    bram_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['BRAM_18K'])
                    timing_avail = float(doc['profile']['UserAssignments']['TargetClockPeriod'])

                    dsp_perc = dsp / dsp_avail
                    ff_perc = ff / ff_avail
                    lut_perc = lut / lut_avail
                    bram_perc = bram / bram_avail
                    timing_perc = timing/timing_avail

                    graph_data.append((avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc))

                    print('avg_latency: %d' % avg_latency)
            knum = knum * 2
        knum = 1
        ksize = ksize * 2
    return

def playGame():
    print("please choose your board:")
    print('1.AC7XX  xc7a200tfbg676-2         DSP 720') 
    print('2.VCU118 xcvu9p-flga2104-2L-e-es1 DSP 6840')
    print('3.VCU110 xcvu190-flgc2104-2-e     DSP 1800')
    print('4.VCU108 xcvu095-ffva2104-2-e     DSP 768')
    print('5.KCU105 xcku040-ffva1156-2-e     DSP 1920')
    print('6.ZC706 xc7z045ffg900-2           DSP 900')
    print('7.Zedboard xc7z020clg484-1        DSP 220')
    print('8.VC709 xc7vx690tffg1761-2        DSP 3600')
    print('9.VC707 xc7vx485tffg1761-2        DSP 2800')
    print('10.KC705 xc7k325tffg900-2          DSP 840')
    #board = int(input())-1
    board = 2 # chose VCU110
    """Paly the pong game"""
    print("please choose your Matrix dimension:")
    print("matrix dimension 1 (D1):")
    #m_dim1 = int(input())
    m_dim1 = 10
    print("matrix dimension 2 (D2):")
    m_dim2 = 10
    #m_dim2 = int(input())
    print("Finding configuration for matrix ({0},{1}) vector ({1},1) multiplication ".format(m_dim2,m_dim1))
    # configure HLS environment
    os.system('source /opt/Xilinx/Vivado_HLS/2017.1/bin/setupEnv.sh')

    search(board, int(2**m_dim1), int(2**m_dim2))

    #graph = np.array(graph_data)
    #np.save('./graph_'+FPGAs[board]+'_'+str(m_dim1)+'_'+str(m_dim2)+'_search', graph)

    #fig, ax = plt.subplots()
    #ax.scatter(graph[:,0], graph[:,1])
    #plt.ylabel('DSP usage %')
    #plt.xlabel('Latency')

    #for i in range(len(graph)):
        #ax.annotate(i, (graph[i,0], graph[i,1]))
    #plt.show()


def main():
    """ Main function """

    playGame()


if __name__ == "__main__":
    main()
