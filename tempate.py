
import math as m
import sys

def kernel(ksize,knum):
  print("void kernel(float w[K_SIZE][K_NUM], float x[K_SIZE],float out[K_NUM]){")
  print("#pragma HLS ARRAY_PARTITION variable=w cyclic dim=1 factor = {0}".format(1*ksize))
  if knum !=1:
    print("#pragma HLS ARRAY_PARTITION variable=w cyclic dim=2 factor = {0}".format(knum))
  print("#pragma HLS ARRAY_PARTITION variable=x cyclic dim=1 factor = {0}".format(1*ksize))
  if knum !=1:
    print("#pragma HLS ARRAY_PARTITION variable=out cyclic dim=1 factor = {0}".format(knum))

  print()
  for i in range(knum):
    print("out[{1}]=compute_engine{0}(".format(ksize, i), end="")
    print("x[{0}],w[{0}][{1}]".format(0, i), end="")
    for k in range(1,ksize):
      print(",\n x[{0}],w[{0}][{1}]".format(k, i), end='')
    print(");")
  print("}")
  print()
  return



def ce(fac):
  print("float compute_engine{}(".format(fac))
  print("float a{0}, float b{0}".format(1),end="")
  for i in range(2,fac+1):
    print(",\nfloat a{0}, float b{0}".format(i),end="")
    
  print("){")
  print("#pragma HLS PIPELINE")
  print("   float m1",end="")
  for i in range(2,fac+1):
    if i%5==0:
      print(",\n    m{0}".format(i),end="")
    else:
      print(",  m{0}".format(i),end="")
  print(";")
  
  print()
  
  print("   float add1",end="")
  for i in range(2,fac-1):
    if i%5==0:
      print(",\n    add{0}".format(i),end="")
    else:
      print(",  add{0}".format(i),end="")
  print(";")
  c=fac
  for i in range(c):
    print("   m{0} = a{0}*b{0};".format(i+1))
  c=c//2
  for i in range(c):
    print("   add{0} = m{1}+m{2};".format(i+1,2*i+1,2*i+2))
  a=c+1 
  count = c
  temp =c
  f=c//2
  for i in range(fac//2-2):
    print("   add{0} = add{1}+add{2};".format(a,temp,temp-1))
    temp=temp-2
    a+=1
    f-=1
    if f==0:
      c=c//2
      f=c//2
      count = count+c
      temp=count
      a=count+1
  print(  " return add{0}+add{1};".format(fac-3,fac-2))
  print("}")
  print()
  return 

def matrix(knum, m_dim1, m_dim2):
  print("void mvm(float weight[M_dim1][M_dim2],  float x[M_dim1],float mout[M_dim2]){");
  print("#pragma HLS ARRAY_PARTITION variable=x cyclic dim=1 factor = 8")
  print("#pragma HLS ARRAY_PARTITION variable=mout cyclic dim=1 factor = 8")
  print("  int i,j,k,l,kk;")
  print("  int index=0;")
  print("  for(k=0;k<%d;k+=K_SIZE){" % m_dim1)
  print("    for(i=0; i< K_SIZE;i++){")
  print("       kvector[i]=x[i+k];")
  print("     }")
  print("    for(l=0;l<M_dim2;l+=K_NUM){")
  print("     #pragma HLS pipeline")
  print("      for(i=0;i< K_SIZE; i++){")
  print("        for (j=0; j < K_NUM; j++){")
  print('        kmatrix[i][j]= weight[k+i][l+j];')
  print("       }")
  print('      }')
  print("      kernel(kmatrix, kvector,kout);")
  for i in range(knum):
    print("       mout[l+{0}]+=kout[{0}];".format(i))
  print("    }")
  print("  }")
  print("}")
  print()

def main(knum, ksize, m_dim1, m_dim2):
    print("#include <ap_fixed.h>")
    print("#define K_SIZE {}".format(ksize))
    print("#define K_NUM {}".format(knum))
    print("#define M_dim1 {}".format(m_dim1))
    print("#define M_dim2 {}".format(m_dim2))
    print()
    print("typedef union {")
    print("  unsigned int i;")
    print("  float f;")
    print(" } u;")
    print()
    print("float kmatrix[K_SIZE][K_NUM];")
    print("float kvector[K_SIZE];")
    print("float kout[K_NUM];")
    print()
    print("float trans_i(ap_int<32> a){")
    print(" u temp;")
    print(" temp.i=a;")
    print(" return temp.f;}")
    print()
    ce(ksize)
    kernel(ksize,knum)
    matrix(knum, m_dim1, m_dim2)

if __name__ == '__main__':
    knum = int(sys.argv[1])
    ksize = int(sys.argv[2])
    m_dim1 = int(sys.argv[3])
    m_dim2 = int(sys.argv[4])
    main(knum, ksize, m_dim1, m_dim2)
