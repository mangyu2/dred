#include <ap_fixed.h>
#define K_SIZE 256
#define K_NUM 1
#define M_dim1 1024
#define M_dim2 1024

typedef union {
  unsigned int i;
  float f;
 } u;

float kmatrix[K_SIZE][K_NUM];
float kvector[K_SIZE];
float kout[K_NUM];

float trans_i(ap_int<32> a){
 u temp;
 temp.i=a;
 return temp.f;}

float compute_engine256(
float a1, float b1,
float a2, float b2,
float a3, float b3,
float a4, float b4,
float a5, float b5,
float a6, float b6,
float a7, float b7,
float a8, float b8,
float a9, float b9,
float a10, float b10,
float a11, float b11,
float a12, float b12,
float a13, float b13,
float a14, float b14,
float a15, float b15,
float a16, float b16,
float a17, float b17,
float a18, float b18,
float a19, float b19,
float a20, float b20,
float a21, float b21,
float a22, float b22,
float a23, float b23,
float a24, float b24,
float a25, float b25,
float a26, float b26,
float a27, float b27,
float a28, float b28,
float a29, float b29,
float a30, float b30,
float a31, float b31,
float a32, float b32,
float a33, float b33,
float a34, float b34,
float a35, float b35,
float a36, float b36,
float a37, float b37,
float a38, float b38,
float a39, float b39,
float a40, float b40,
float a41, float b41,
float a42, float b42,
float a43, float b43,
float a44, float b44,
float a45, float b45,
float a46, float b46,
float a47, float b47,
float a48, float b48,
float a49, float b49,
float a50, float b50,
float a51, float b51,
float a52, float b52,
float a53, float b53,
float a54, float b54,
float a55, float b55,
float a56, float b56,
float a57, float b57,
float a58, float b58,
float a59, float b59,
float a60, float b60,
float a61, float b61,
float a62, float b62,
float a63, float b63,
float a64, float b64,
float a65, float b65,
float a66, float b66,
float a67, float b67,
float a68, float b68,
float a69, float b69,
float a70, float b70,
float a71, float b71,
float a72, float b72,
float a73, float b73,
float a74, float b74,
float a75, float b75,
float a76, float b76,
float a77, float b77,
float a78, float b78,
float a79, float b79,
float a80, float b80,
float a81, float b81,
float a82, float b82,
float a83, float b83,
float a84, float b84,
float a85, float b85,
float a86, float b86,
float a87, float b87,
float a88, float b88,
float a89, float b89,
float a90, float b90,
float a91, float b91,
float a92, float b92,
float a93, float b93,
float a94, float b94,
float a95, float b95,
float a96, float b96,
float a97, float b97,
float a98, float b98,
float a99, float b99,
float a100, float b100,
float a101, float b101,
float a102, float b102,
float a103, float b103,
float a104, float b104,
float a105, float b105,
float a106, float b106,
float a107, float b107,
float a108, float b108,
float a109, float b109,
float a110, float b110,
float a111, float b111,
float a112, float b112,
float a113, float b113,
float a114, float b114,
float a115, float b115,
float a116, float b116,
float a117, float b117,
float a118, float b118,
float a119, float b119,
float a120, float b120,
float a121, float b121,
float a122, float b122,
float a123, float b123,
float a124, float b124,
float a125, float b125,
float a126, float b126,
float a127, float b127,
float a128, float b128,
float a129, float b129,
float a130, float b130,
float a131, float b131,
float a132, float b132,
float a133, float b133,
float a134, float b134,
float a135, float b135,
float a136, float b136,
float a137, float b137,
float a138, float b138,
float a139, float b139,
float a140, float b140,
float a141, float b141,
float a142, float b142,
float a143, float b143,
float a144, float b144,
float a145, float b145,
float a146, float b146,
float a147, float b147,
float a148, float b148,
float a149, float b149,
float a150, float b150,
float a151, float b151,
float a152, float b152,
float a153, float b153,
float a154, float b154,
float a155, float b155,
float a156, float b156,
float a157, float b157,
float a158, float b158,
float a159, float b159,
float a160, float b160,
float a161, float b161,
float a162, float b162,
float a163, float b163,
float a164, float b164,
float a165, float b165,
float a166, float b166,
float a167, float b167,
float a168, float b168,
float a169, float b169,
float a170, float b170,
float a171, float b171,
float a172, float b172,
float a173, float b173,
float a174, float b174,
float a175, float b175,
float a176, float b176,
float a177, float b177,
float a178, float b178,
float a179, float b179,
float a180, float b180,
float a181, float b181,
float a182, float b182,
float a183, float b183,
float a184, float b184,
float a185, float b185,
float a186, float b186,
float a187, float b187,
float a188, float b188,
float a189, float b189,
float a190, float b190,
float a191, float b191,
float a192, float b192,
float a193, float b193,
float a194, float b194,
float a195, float b195,
float a196, float b196,
float a197, float b197,
float a198, float b198,
float a199, float b199,
float a200, float b200,
float a201, float b201,
float a202, float b202,
float a203, float b203,
float a204, float b204,
float a205, float b205,
float a206, float b206,
float a207, float b207,
float a208, float b208,
float a209, float b209,
float a210, float b210,
float a211, float b211,
float a212, float b212,
float a213, float b213,
float a214, float b214,
float a215, float b215,
float a216, float b216,
float a217, float b217,
float a218, float b218,
float a219, float b219,
float a220, float b220,
float a221, float b221,
float a222, float b222,
float a223, float b223,
float a224, float b224,
float a225, float b225,
float a226, float b226,
float a227, float b227,
float a228, float b228,
float a229, float b229,
float a230, float b230,
float a231, float b231,
float a232, float b232,
float a233, float b233,
float a234, float b234,
float a235, float b235,
float a236, float b236,
float a237, float b237,
float a238, float b238,
float a239, float b239,
float a240, float b240,
float a241, float b241,
float a242, float b242,
float a243, float b243,
float a244, float b244,
float a245, float b245,
float a246, float b246,
float a247, float b247,
float a248, float b248,
float a249, float b249,
float a250, float b250,
float a251, float b251,
float a252, float b252,
float a253, float b253,
float a254, float b254,
float a255, float b255,
float a256, float b256){
#pragma HLS PIPELINE
   float m1,  m2,  m3,  m4,
    m5,  m6,  m7,  m8,  m9,
    m10,  m11,  m12,  m13,  m14,
    m15,  m16,  m17,  m18,  m19,
    m20,  m21,  m22,  m23,  m24,
    m25,  m26,  m27,  m28,  m29,
    m30,  m31,  m32,  m33,  m34,
    m35,  m36,  m37,  m38,  m39,
    m40,  m41,  m42,  m43,  m44,
    m45,  m46,  m47,  m48,  m49,
    m50,  m51,  m52,  m53,  m54,
    m55,  m56,  m57,  m58,  m59,
    m60,  m61,  m62,  m63,  m64,
    m65,  m66,  m67,  m68,  m69,
    m70,  m71,  m72,  m73,  m74,
    m75,  m76,  m77,  m78,  m79,
    m80,  m81,  m82,  m83,  m84,
    m85,  m86,  m87,  m88,  m89,
    m90,  m91,  m92,  m93,  m94,
    m95,  m96,  m97,  m98,  m99,
    m100,  m101,  m102,  m103,  m104,
    m105,  m106,  m107,  m108,  m109,
    m110,  m111,  m112,  m113,  m114,
    m115,  m116,  m117,  m118,  m119,
    m120,  m121,  m122,  m123,  m124,
    m125,  m126,  m127,  m128,  m129,
    m130,  m131,  m132,  m133,  m134,
    m135,  m136,  m137,  m138,  m139,
    m140,  m141,  m142,  m143,  m144,
    m145,  m146,  m147,  m148,  m149,
    m150,  m151,  m152,  m153,  m154,
    m155,  m156,  m157,  m158,  m159,
    m160,  m161,  m162,  m163,  m164,
    m165,  m166,  m167,  m168,  m169,
    m170,  m171,  m172,  m173,  m174,
    m175,  m176,  m177,  m178,  m179,
    m180,  m181,  m182,  m183,  m184,
    m185,  m186,  m187,  m188,  m189,
    m190,  m191,  m192,  m193,  m194,
    m195,  m196,  m197,  m198,  m199,
    m200,  m201,  m202,  m203,  m204,
    m205,  m206,  m207,  m208,  m209,
    m210,  m211,  m212,  m213,  m214,
    m215,  m216,  m217,  m218,  m219,
    m220,  m221,  m222,  m223,  m224,
    m225,  m226,  m227,  m228,  m229,
    m230,  m231,  m232,  m233,  m234,
    m235,  m236,  m237,  m238,  m239,
    m240,  m241,  m242,  m243,  m244,
    m245,  m246,  m247,  m248,  m249,
    m250,  m251,  m252,  m253,  m254,
    m255,  m256;

   float add1,  add2,  add3,  add4,
    add5,  add6,  add7,  add8,  add9,
    add10,  add11,  add12,  add13,  add14,
    add15,  add16,  add17,  add18,  add19,
    add20,  add21,  add22,  add23,  add24,
    add25,  add26,  add27,  add28,  add29,
    add30,  add31,  add32,  add33,  add34,
    add35,  add36,  add37,  add38,  add39,
    add40,  add41,  add42,  add43,  add44,
    add45,  add46,  add47,  add48,  add49,
    add50,  add51,  add52,  add53,  add54,
    add55,  add56,  add57,  add58,  add59,
    add60,  add61,  add62,  add63,  add64,
    add65,  add66,  add67,  add68,  add69,
    add70,  add71,  add72,  add73,  add74,
    add75,  add76,  add77,  add78,  add79,
    add80,  add81,  add82,  add83,  add84,
    add85,  add86,  add87,  add88,  add89,
    add90,  add91,  add92,  add93,  add94,
    add95,  add96,  add97,  add98,  add99,
    add100,  add101,  add102,  add103,  add104,
    add105,  add106,  add107,  add108,  add109,
    add110,  add111,  add112,  add113,  add114,
    add115,  add116,  add117,  add118,  add119,
    add120,  add121,  add122,  add123,  add124,
    add125,  add126,  add127,  add128,  add129,
    add130,  add131,  add132,  add133,  add134,
    add135,  add136,  add137,  add138,  add139,
    add140,  add141,  add142,  add143,  add144,
    add145,  add146,  add147,  add148,  add149,
    add150,  add151,  add152,  add153,  add154,
    add155,  add156,  add157,  add158,  add159,
    add160,  add161,  add162,  add163,  add164,
    add165,  add166,  add167,  add168,  add169,
    add170,  add171,  add172,  add173,  add174,
    add175,  add176,  add177,  add178,  add179,
    add180,  add181,  add182,  add183,  add184,
    add185,  add186,  add187,  add188,  add189,
    add190,  add191,  add192,  add193,  add194,
    add195,  add196,  add197,  add198,  add199,
    add200,  add201,  add202,  add203,  add204,
    add205,  add206,  add207,  add208,  add209,
    add210,  add211,  add212,  add213,  add214,
    add215,  add216,  add217,  add218,  add219,
    add220,  add221,  add222,  add223,  add224,
    add225,  add226,  add227,  add228,  add229,
    add230,  add231,  add232,  add233,  add234,
    add235,  add236,  add237,  add238,  add239,
    add240,  add241,  add242,  add243,  add244,
    add245,  add246,  add247,  add248,  add249,
    add250,  add251,  add252,  add253,  add254;
   m1 = a1*b1;
   m2 = a2*b2;
   m3 = a3*b3;
   m4 = a4*b4;
   m5 = a5*b5;
   m6 = a6*b6;
   m7 = a7*b7;
   m8 = a8*b8;
   m9 = a9*b9;
   m10 = a10*b10;
   m11 = a11*b11;
   m12 = a12*b12;
   m13 = a13*b13;
   m14 = a14*b14;
   m15 = a15*b15;
   m16 = a16*b16;
   m17 = a17*b17;
   m18 = a18*b18;
   m19 = a19*b19;
   m20 = a20*b20;
   m21 = a21*b21;
   m22 = a22*b22;
   m23 = a23*b23;
   m24 = a24*b24;
   m25 = a25*b25;
   m26 = a26*b26;
   m27 = a27*b27;
   m28 = a28*b28;
   m29 = a29*b29;
   m30 = a30*b30;
   m31 = a31*b31;
   m32 = a32*b32;
   m33 = a33*b33;
   m34 = a34*b34;
   m35 = a35*b35;
   m36 = a36*b36;
   m37 = a37*b37;
   m38 = a38*b38;
   m39 = a39*b39;
   m40 = a40*b40;
   m41 = a41*b41;
   m42 = a42*b42;
   m43 = a43*b43;
   m44 = a44*b44;
   m45 = a45*b45;
   m46 = a46*b46;
   m47 = a47*b47;
   m48 = a48*b48;
   m49 = a49*b49;
   m50 = a50*b50;
   m51 = a51*b51;
   m52 = a52*b52;
   m53 = a53*b53;
   m54 = a54*b54;
   m55 = a55*b55;
   m56 = a56*b56;
   m57 = a57*b57;
   m58 = a58*b58;
   m59 = a59*b59;
   m60 = a60*b60;
   m61 = a61*b61;
   m62 = a62*b62;
   m63 = a63*b63;
   m64 = a64*b64;
   m65 = a65*b65;
   m66 = a66*b66;
   m67 = a67*b67;
   m68 = a68*b68;
   m69 = a69*b69;
   m70 = a70*b70;
   m71 = a71*b71;
   m72 = a72*b72;
   m73 = a73*b73;
   m74 = a74*b74;
   m75 = a75*b75;
   m76 = a76*b76;
   m77 = a77*b77;
   m78 = a78*b78;
   m79 = a79*b79;
   m80 = a80*b80;
   m81 = a81*b81;
   m82 = a82*b82;
   m83 = a83*b83;
   m84 = a84*b84;
   m85 = a85*b85;
   m86 = a86*b86;
   m87 = a87*b87;
   m88 = a88*b88;
   m89 = a89*b89;
   m90 = a90*b90;
   m91 = a91*b91;
   m92 = a92*b92;
   m93 = a93*b93;
   m94 = a94*b94;
   m95 = a95*b95;
   m96 = a96*b96;
   m97 = a97*b97;
   m98 = a98*b98;
   m99 = a99*b99;
   m100 = a100*b100;
   m101 = a101*b101;
   m102 = a102*b102;
   m103 = a103*b103;
   m104 = a104*b104;
   m105 = a105*b105;
   m106 = a106*b106;
   m107 = a107*b107;
   m108 = a108*b108;
   m109 = a109*b109;
   m110 = a110*b110;
   m111 = a111*b111;
   m112 = a112*b112;
   m113 = a113*b113;
   m114 = a114*b114;
   m115 = a115*b115;
   m116 = a116*b116;
   m117 = a117*b117;
   m118 = a118*b118;
   m119 = a119*b119;
   m120 = a120*b120;
   m121 = a121*b121;
   m122 = a122*b122;
   m123 = a123*b123;
   m124 = a124*b124;
   m125 = a125*b125;
   m126 = a126*b126;
   m127 = a127*b127;
   m128 = a128*b128;
   m129 = a129*b129;
   m130 = a130*b130;
   m131 = a131*b131;
   m132 = a132*b132;
   m133 = a133*b133;
   m134 = a134*b134;
   m135 = a135*b135;
   m136 = a136*b136;
   m137 = a137*b137;
   m138 = a138*b138;
   m139 = a139*b139;
   m140 = a140*b140;
   m141 = a141*b141;
   m142 = a142*b142;
   m143 = a143*b143;
   m144 = a144*b144;
   m145 = a145*b145;
   m146 = a146*b146;
   m147 = a147*b147;
   m148 = a148*b148;
   m149 = a149*b149;
   m150 = a150*b150;
   m151 = a151*b151;
   m152 = a152*b152;
   m153 = a153*b153;
   m154 = a154*b154;
   m155 = a155*b155;
   m156 = a156*b156;
   m157 = a157*b157;
   m158 = a158*b158;
   m159 = a159*b159;
   m160 = a160*b160;
   m161 = a161*b161;
   m162 = a162*b162;
   m163 = a163*b163;
   m164 = a164*b164;
   m165 = a165*b165;
   m166 = a166*b166;
   m167 = a167*b167;
   m168 = a168*b168;
   m169 = a169*b169;
   m170 = a170*b170;
   m171 = a171*b171;
   m172 = a172*b172;
   m173 = a173*b173;
   m174 = a174*b174;
   m175 = a175*b175;
   m176 = a176*b176;
   m177 = a177*b177;
   m178 = a178*b178;
   m179 = a179*b179;
   m180 = a180*b180;
   m181 = a181*b181;
   m182 = a182*b182;
   m183 = a183*b183;
   m184 = a184*b184;
   m185 = a185*b185;
   m186 = a186*b186;
   m187 = a187*b187;
   m188 = a188*b188;
   m189 = a189*b189;
   m190 = a190*b190;
   m191 = a191*b191;
   m192 = a192*b192;
   m193 = a193*b193;
   m194 = a194*b194;
   m195 = a195*b195;
   m196 = a196*b196;
   m197 = a197*b197;
   m198 = a198*b198;
   m199 = a199*b199;
   m200 = a200*b200;
   m201 = a201*b201;
   m202 = a202*b202;
   m203 = a203*b203;
   m204 = a204*b204;
   m205 = a205*b205;
   m206 = a206*b206;
   m207 = a207*b207;
   m208 = a208*b208;
   m209 = a209*b209;
   m210 = a210*b210;
   m211 = a211*b211;
   m212 = a212*b212;
   m213 = a213*b213;
   m214 = a214*b214;
   m215 = a215*b215;
   m216 = a216*b216;
   m217 = a217*b217;
   m218 = a218*b218;
   m219 = a219*b219;
   m220 = a220*b220;
   m221 = a221*b221;
   m222 = a222*b222;
   m223 = a223*b223;
   m224 = a224*b224;
   m225 = a225*b225;
   m226 = a226*b226;
   m227 = a227*b227;
   m228 = a228*b228;
   m229 = a229*b229;
   m230 = a230*b230;
   m231 = a231*b231;
   m232 = a232*b232;
   m233 = a233*b233;
   m234 = a234*b234;
   m235 = a235*b235;
   m236 = a236*b236;
   m237 = a237*b237;
   m238 = a238*b238;
   m239 = a239*b239;
   m240 = a240*b240;
   m241 = a241*b241;
   m242 = a242*b242;
   m243 = a243*b243;
   m244 = a244*b244;
   m245 = a245*b245;
   m246 = a246*b246;
   m247 = a247*b247;
   m248 = a248*b248;
   m249 = a249*b249;
   m250 = a250*b250;
   m251 = a251*b251;
   m252 = a252*b252;
   m253 = a253*b253;
   m254 = a254*b254;
   m255 = a255*b255;
   m256 = a256*b256;
   add1 = m1+m2;
   add2 = m3+m4;
   add3 = m5+m6;
   add4 = m7+m8;
   add5 = m9+m10;
   add6 = m11+m12;
   add7 = m13+m14;
   add8 = m15+m16;
   add9 = m17+m18;
   add10 = m19+m20;
   add11 = m21+m22;
   add12 = m23+m24;
   add13 = m25+m26;
   add14 = m27+m28;
   add15 = m29+m30;
   add16 = m31+m32;
   add17 = m33+m34;
   add18 = m35+m36;
   add19 = m37+m38;
   add20 = m39+m40;
   add21 = m41+m42;
   add22 = m43+m44;
   add23 = m45+m46;
   add24 = m47+m48;
   add25 = m49+m50;
   add26 = m51+m52;
   add27 = m53+m54;
   add28 = m55+m56;
   add29 = m57+m58;
   add30 = m59+m60;
   add31 = m61+m62;
   add32 = m63+m64;
   add33 = m65+m66;
   add34 = m67+m68;
   add35 = m69+m70;
   add36 = m71+m72;
   add37 = m73+m74;
   add38 = m75+m76;
   add39 = m77+m78;
   add40 = m79+m80;
   add41 = m81+m82;
   add42 = m83+m84;
   add43 = m85+m86;
   add44 = m87+m88;
   add45 = m89+m90;
   add46 = m91+m92;
   add47 = m93+m94;
   add48 = m95+m96;
   add49 = m97+m98;
   add50 = m99+m100;
   add51 = m101+m102;
   add52 = m103+m104;
   add53 = m105+m106;
   add54 = m107+m108;
   add55 = m109+m110;
   add56 = m111+m112;
   add57 = m113+m114;
   add58 = m115+m116;
   add59 = m117+m118;
   add60 = m119+m120;
   add61 = m121+m122;
   add62 = m123+m124;
   add63 = m125+m126;
   add64 = m127+m128;
   add65 = m129+m130;
   add66 = m131+m132;
   add67 = m133+m134;
   add68 = m135+m136;
   add69 = m137+m138;
   add70 = m139+m140;
   add71 = m141+m142;
   add72 = m143+m144;
   add73 = m145+m146;
   add74 = m147+m148;
   add75 = m149+m150;
   add76 = m151+m152;
   add77 = m153+m154;
   add78 = m155+m156;
   add79 = m157+m158;
   add80 = m159+m160;
   add81 = m161+m162;
   add82 = m163+m164;
   add83 = m165+m166;
   add84 = m167+m168;
   add85 = m169+m170;
   add86 = m171+m172;
   add87 = m173+m174;
   add88 = m175+m176;
   add89 = m177+m178;
   add90 = m179+m180;
   add91 = m181+m182;
   add92 = m183+m184;
   add93 = m185+m186;
   add94 = m187+m188;
   add95 = m189+m190;
   add96 = m191+m192;
   add97 = m193+m194;
   add98 = m195+m196;
   add99 = m197+m198;
   add100 = m199+m200;
   add101 = m201+m202;
   add102 = m203+m204;
   add103 = m205+m206;
   add104 = m207+m208;
   add105 = m209+m210;
   add106 = m211+m212;
   add107 = m213+m214;
   add108 = m215+m216;
   add109 = m217+m218;
   add110 = m219+m220;
   add111 = m221+m222;
   add112 = m223+m224;
   add113 = m225+m226;
   add114 = m227+m228;
   add115 = m229+m230;
   add116 = m231+m232;
   add117 = m233+m234;
   add118 = m235+m236;
   add119 = m237+m238;
   add120 = m239+m240;
   add121 = m241+m242;
   add122 = m243+m244;
   add123 = m245+m246;
   add124 = m247+m248;
   add125 = m249+m250;
   add126 = m251+m252;
   add127 = m253+m254;
   add128 = m255+m256;
   add129 = add128+add127;
   add130 = add126+add125;
   add131 = add124+add123;
   add132 = add122+add121;
   add133 = add120+add119;
   add134 = add118+add117;
   add135 = add116+add115;
   add136 = add114+add113;
   add137 = add112+add111;
   add138 = add110+add109;
   add139 = add108+add107;
   add140 = add106+add105;
   add141 = add104+add103;
   add142 = add102+add101;
   add143 = add100+add99;
   add144 = add98+add97;
   add145 = add96+add95;
   add146 = add94+add93;
   add147 = add92+add91;
   add148 = add90+add89;
   add149 = add88+add87;
   add150 = add86+add85;
   add151 = add84+add83;
   add152 = add82+add81;
   add153 = add80+add79;
   add154 = add78+add77;
   add155 = add76+add75;
   add156 = add74+add73;
   add157 = add72+add71;
   add158 = add70+add69;
   add159 = add68+add67;
   add160 = add66+add65;
   add161 = add64+add63;
   add162 = add62+add61;
   add163 = add60+add59;
   add164 = add58+add57;
   add165 = add56+add55;
   add166 = add54+add53;
   add167 = add52+add51;
   add168 = add50+add49;
   add169 = add48+add47;
   add170 = add46+add45;
   add171 = add44+add43;
   add172 = add42+add41;
   add173 = add40+add39;
   add174 = add38+add37;
   add175 = add36+add35;
   add176 = add34+add33;
   add177 = add32+add31;
   add178 = add30+add29;
   add179 = add28+add27;
   add180 = add26+add25;
   add181 = add24+add23;
   add182 = add22+add21;
   add183 = add20+add19;
   add184 = add18+add17;
   add185 = add16+add15;
   add186 = add14+add13;
   add187 = add12+add11;
   add188 = add10+add9;
   add189 = add8+add7;
   add190 = add6+add5;
   add191 = add4+add3;
   add192 = add2+add1;
   add193 = add192+add191;
   add194 = add190+add189;
   add195 = add188+add187;
   add196 = add186+add185;
   add197 = add184+add183;
   add198 = add182+add181;
   add199 = add180+add179;
   add200 = add178+add177;
   add201 = add176+add175;
   add202 = add174+add173;
   add203 = add172+add171;
   add204 = add170+add169;
   add205 = add168+add167;
   add206 = add166+add165;
   add207 = add164+add163;
   add208 = add162+add161;
   add209 = add160+add159;
   add210 = add158+add157;
   add211 = add156+add155;
   add212 = add154+add153;
   add213 = add152+add151;
   add214 = add150+add149;
   add215 = add148+add147;
   add216 = add146+add145;
   add217 = add144+add143;
   add218 = add142+add141;
   add219 = add140+add139;
   add220 = add138+add137;
   add221 = add136+add135;
   add222 = add134+add133;
   add223 = add132+add131;
   add224 = add130+add129;
   add225 = add224+add223;
   add226 = add222+add221;
   add227 = add220+add219;
   add228 = add218+add217;
   add229 = add216+add215;
   add230 = add214+add213;
   add231 = add212+add211;
   add232 = add210+add209;
   add233 = add208+add207;
   add234 = add206+add205;
   add235 = add204+add203;
   add236 = add202+add201;
   add237 = add200+add199;
   add238 = add198+add197;
   add239 = add196+add195;
   add240 = add194+add193;
   add241 = add240+add239;
   add242 = add238+add237;
   add243 = add236+add235;
   add244 = add234+add233;
   add245 = add232+add231;
   add246 = add230+add229;
   add247 = add228+add227;
   add248 = add226+add225;
   add249 = add248+add247;
   add250 = add246+add245;
   add251 = add244+add243;
   add252 = add242+add241;
   add253 = add252+add251;
   add254 = add250+add249;
 return add253+add254;
}

void kernel(float w[K_SIZE][K_NUM], float x[K_SIZE],float out[K_NUM]){
#pragma HLS ARRAY_PARTITION variable=w cyclic dim=1 factor = 256
#pragma HLS ARRAY_PARTITION variable=x cyclic dim=1 factor = 256

out[0]=compute_engine256(x[0],w[0][0],
 x[1],w[1][0],
 x[2],w[2][0],
 x[3],w[3][0],
 x[4],w[4][0],
 x[5],w[5][0],
 x[6],w[6][0],
 x[7],w[7][0],
 x[8],w[8][0],
 x[9],w[9][0],
 x[10],w[10][0],
 x[11],w[11][0],
 x[12],w[12][0],
 x[13],w[13][0],
 x[14],w[14][0],
 x[15],w[15][0],
 x[16],w[16][0],
 x[17],w[17][0],
 x[18],w[18][0],
 x[19],w[19][0],
 x[20],w[20][0],
 x[21],w[21][0],
 x[22],w[22][0],
 x[23],w[23][0],
 x[24],w[24][0],
 x[25],w[25][0],
 x[26],w[26][0],
 x[27],w[27][0],
 x[28],w[28][0],
 x[29],w[29][0],
 x[30],w[30][0],
 x[31],w[31][0],
 x[32],w[32][0],
 x[33],w[33][0],
 x[34],w[34][0],
 x[35],w[35][0],
 x[36],w[36][0],
 x[37],w[37][0],
 x[38],w[38][0],
 x[39],w[39][0],
 x[40],w[40][0],
 x[41],w[41][0],
 x[42],w[42][0],
 x[43],w[43][0],
 x[44],w[44][0],
 x[45],w[45][0],
 x[46],w[46][0],
 x[47],w[47][0],
 x[48],w[48][0],
 x[49],w[49][0],
 x[50],w[50][0],
 x[51],w[51][0],
 x[52],w[52][0],
 x[53],w[53][0],
 x[54],w[54][0],
 x[55],w[55][0],
 x[56],w[56][0],
 x[57],w[57][0],
 x[58],w[58][0],
 x[59],w[59][0],
 x[60],w[60][0],
 x[61],w[61][0],
 x[62],w[62][0],
 x[63],w[63][0],
 x[64],w[64][0],
 x[65],w[65][0],
 x[66],w[66][0],
 x[67],w[67][0],
 x[68],w[68][0],
 x[69],w[69][0],
 x[70],w[70][0],
 x[71],w[71][0],
 x[72],w[72][0],
 x[73],w[73][0],
 x[74],w[74][0],
 x[75],w[75][0],
 x[76],w[76][0],
 x[77],w[77][0],
 x[78],w[78][0],
 x[79],w[79][0],
 x[80],w[80][0],
 x[81],w[81][0],
 x[82],w[82][0],
 x[83],w[83][0],
 x[84],w[84][0],
 x[85],w[85][0],
 x[86],w[86][0],
 x[87],w[87][0],
 x[88],w[88][0],
 x[89],w[89][0],
 x[90],w[90][0],
 x[91],w[91][0],
 x[92],w[92][0],
 x[93],w[93][0],
 x[94],w[94][0],
 x[95],w[95][0],
 x[96],w[96][0],
 x[97],w[97][0],
 x[98],w[98][0],
 x[99],w[99][0],
 x[100],w[100][0],
 x[101],w[101][0],
 x[102],w[102][0],
 x[103],w[103][0],
 x[104],w[104][0],
 x[105],w[105][0],
 x[106],w[106][0],
 x[107],w[107][0],
 x[108],w[108][0],
 x[109],w[109][0],
 x[110],w[110][0],
 x[111],w[111][0],
 x[112],w[112][0],
 x[113],w[113][0],
 x[114],w[114][0],
 x[115],w[115][0],
 x[116],w[116][0],
 x[117],w[117][0],
 x[118],w[118][0],
 x[119],w[119][0],
 x[120],w[120][0],
 x[121],w[121][0],
 x[122],w[122][0],
 x[123],w[123][0],
 x[124],w[124][0],
 x[125],w[125][0],
 x[126],w[126][0],
 x[127],w[127][0],
 x[128],w[128][0],
 x[129],w[129][0],
 x[130],w[130][0],
 x[131],w[131][0],
 x[132],w[132][0],
 x[133],w[133][0],
 x[134],w[134][0],
 x[135],w[135][0],
 x[136],w[136][0],
 x[137],w[137][0],
 x[138],w[138][0],
 x[139],w[139][0],
 x[140],w[140][0],
 x[141],w[141][0],
 x[142],w[142][0],
 x[143],w[143][0],
 x[144],w[144][0],
 x[145],w[145][0],
 x[146],w[146][0],
 x[147],w[147][0],
 x[148],w[148][0],
 x[149],w[149][0],
 x[150],w[150][0],
 x[151],w[151][0],
 x[152],w[152][0],
 x[153],w[153][0],
 x[154],w[154][0],
 x[155],w[155][0],
 x[156],w[156][0],
 x[157],w[157][0],
 x[158],w[158][0],
 x[159],w[159][0],
 x[160],w[160][0],
 x[161],w[161][0],
 x[162],w[162][0],
 x[163],w[163][0],
 x[164],w[164][0],
 x[165],w[165][0],
 x[166],w[166][0],
 x[167],w[167][0],
 x[168],w[168][0],
 x[169],w[169][0],
 x[170],w[170][0],
 x[171],w[171][0],
 x[172],w[172][0],
 x[173],w[173][0],
 x[174],w[174][0],
 x[175],w[175][0],
 x[176],w[176][0],
 x[177],w[177][0],
 x[178],w[178][0],
 x[179],w[179][0],
 x[180],w[180][0],
 x[181],w[181][0],
 x[182],w[182][0],
 x[183],w[183][0],
 x[184],w[184][0],
 x[185],w[185][0],
 x[186],w[186][0],
 x[187],w[187][0],
 x[188],w[188][0],
 x[189],w[189][0],
 x[190],w[190][0],
 x[191],w[191][0],
 x[192],w[192][0],
 x[193],w[193][0],
 x[194],w[194][0],
 x[195],w[195][0],
 x[196],w[196][0],
 x[197],w[197][0],
 x[198],w[198][0],
 x[199],w[199][0],
 x[200],w[200][0],
 x[201],w[201][0],
 x[202],w[202][0],
 x[203],w[203][0],
 x[204],w[204][0],
 x[205],w[205][0],
 x[206],w[206][0],
 x[207],w[207][0],
 x[208],w[208][0],
 x[209],w[209][0],
 x[210],w[210][0],
 x[211],w[211][0],
 x[212],w[212][0],
 x[213],w[213][0],
 x[214],w[214][0],
 x[215],w[215][0],
 x[216],w[216][0],
 x[217],w[217][0],
 x[218],w[218][0],
 x[219],w[219][0],
 x[220],w[220][0],
 x[221],w[221][0],
 x[222],w[222][0],
 x[223],w[223][0],
 x[224],w[224][0],
 x[225],w[225][0],
 x[226],w[226][0],
 x[227],w[227][0],
 x[228],w[228][0],
 x[229],w[229][0],
 x[230],w[230][0],
 x[231],w[231][0],
 x[232],w[232][0],
 x[233],w[233][0],
 x[234],w[234][0],
 x[235],w[235][0],
 x[236],w[236][0],
 x[237],w[237][0],
 x[238],w[238][0],
 x[239],w[239][0],
 x[240],w[240][0],
 x[241],w[241][0],
 x[242],w[242][0],
 x[243],w[243][0],
 x[244],w[244][0],
 x[245],w[245][0],
 x[246],w[246][0],
 x[247],w[247][0],
 x[248],w[248][0],
 x[249],w[249][0],
 x[250],w[250][0],
 x[251],w[251][0],
 x[252],w[252][0],
 x[253],w[253][0],
 x[254],w[254][0],
 x[255],w[255][0]);
}

void mvm(float weight[M_dim1][M_dim2],  float x[M_dim1],float mout[M_dim2]){
#pragma HLS ARRAY_PARTITION variable=x cyclic dim=1 factor = 8
#pragma HLS ARRAY_PARTITION variable=mout cyclic dim=1 factor = 8
  int i,j,k,l,kk;
  int index=0;
  for(k=0;k<1024;k+=K_SIZE){
    for(i=0; i< K_SIZE;i++){
       kvector[i]=x[i+k];
     }
    for(l=0;l<M_dim2;l+=K_NUM){
     #pragma HLS pipeline
      for(i=0;i< K_SIZE; i++){
        for (j=0; j < K_NUM; j++){
        kmatrix[i][j]= weight[k+i][l+j];
       }
      }
      kernel(kmatrix, kvector,kout);
       mout[l+0]+=kout[0];
    }
  }
}

