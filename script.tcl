############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project hls_proj
set_top mvm
add_files hls_proj/gened.cpp
open_solution "solution1"
set_part xcvu190-flgc2104-2-e
create_clock -period 10 -name default
config_schedule -effort low
#source "./hls_proj/solution1/directives.tcl"
#csim_design -compiler gcc
csynth_design
#cosim_design
#export_design -rtl verilog -format ip_catalog
exit