import math, os, platform, random, re, sys, time, threading, itertools, json, argparse, collections, glob, ast
from jinja2 import Environment, FileSystemLoader, Template
import xmltodict

NUM_FPGA = 10########################################################################################################

FPGAs = [
    'xc7a200tfbg676-2',
    'xcvu9p-flga2104-2L-e-es1',
    'xcvu190-flgc2104-2-e',
    'xcvu095-ffva2104-2-e',
    'xcku040-ffva1156-2-e',
    'xc7z045ffg900-2',
    'xc7z020clg484-1',
    'xc7vx690tffg1761-2',
    'xc7vx485tffg1761-2',
    'xc7k325tffg900-2'
]

def generate_code(template_file, output_file_path, render_dict=None):
    '''
    Generates a string from a template file.
    '''
    cwd = os.getcwd()
    template_dir = os.path.join(cwd, "templates")
    template_env = Environment(loader=FileSystemLoader(template_dir), trim_blocks=True,
                               keep_trailing_newline=True)

    template = template_env.get_template(template_file)
    if render_dict is not None:
        output_as_string = template.render(render_dict)
    else:
        output_as_string = template.render()

    #print(output_as_string)

    with open(output_file_path, 'w') as generate_script_file:
        generate_script_file.write(output_as_string)


def hls_step(a_t, board, param):  # board interface
    # execute vivado HLS
    act1 = a_t % 5
    act2 = (a_t // 5) % 5
    act3 = (a_t // 25) % 5
    out_bound = 0

    param["knum"] = param["knum"] * (2 ^ (act1 - 2))
    param["ksize"] = param["ksize"] * (2 ^ (act2 - 2))
    param["vector_unroll"] = param["vector_unroll"] * (2 ^ (act3 - 2))

    if param["knum"] > 512:
        param["knum"] = 512
        out_bound = 1
    elif param["knum"] < 1:
        param["knum"] = 1
        out_bound = 1
    if param["ksize"] > 512:
        param["ksize"] = 512
        out_bound = 1
    elif param["ksize"] < 4:
        param["ksize"] = 4
        out_bound = 1

    if param["vector_unroll"] > 512:
        param["vector_unroll"] = 512
        out_bound = 1
    elif param["vector_unroll"] < 1:
        param["vector_unroll"] = 1
        out_bound = 1

    ###############################ADDING tempate
    # generate paramterized code
    generate_code("nmt.jinja", "./hls_proj/gened.cpp", param)
'''
    # generate tcl script for the selected FPGA
    generate_code("tcl_script.jinja", "./script.tcl", {'part': FPGAs[board]})

    hls_exit_code = os.system('/opt/Xilinx/Vivado_HLS/2017.1/bin/vivado_hls -f script.tcl')
    if hls_exit_code is not 0:
        print("hls did not exit normally!")
        return

    with open('./hls_proj/solution1/syn/report/mvm512_512_csynth.xml') as fd:
        doc = xmltodict.parse(fd.read())

    avg_latency = int(doc['profile']['PerformanceEstimates']['SummaryOfOverallLatency']['Average-caseLatency'])

    dsp = int(doc['profile']['AreaEstimates']['Resources']['DSP48E'])
    ff = int(doc['profile']['AreaEstimates']['Resources']['FF'])
    lut = int(doc['profile']['AreaEstimates']['Resources']['LUT'])
    bram = int(doc['profile']['AreaEstimates']['Resources']['BRAM_18K'])
    timing = float(doc['profile']['PerformanceEstimates']['SummaryOfTimingAnalysis']['EstimatedClockPeriod'])

    dsp_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['DSP48E'])
    ff_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['FF'])
    lut_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['LUT'])
    bram_avail = int(doc['profile']['AreaEstimates']['AvailableResources']['BRAM_18K'])
    timing_avail = float(doc['profile']['UserAssignments']['TargetClockPeriod'])

    dsp_perc = dsp / dsp_avail
    ff_perc = ff / ff_avail
    lut_perc = lut / lut_avail
    bram_perc = bram / bram_avail
    timing_perc = timing / timing_avail

    list = [avg_latency, dsp_perc, ff_perc, lut_perc, bram_perc, timing_perc, out_bound]
    print(list)
'''

def main ():
    generate_code("nmt.jinja", "./hls_proj/gened.cpp", {'knum':32,'ksize':4,'matrix_dim1':64,'matrix_dim2':512})

if __name__ == '__main__':
    main()

